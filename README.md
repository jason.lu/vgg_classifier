VGG Image Classifier Backend Service
====================================

Derived from the [cpuvisor-srv](https://github.com/kencoken/cpuvisor-srv) project by Ken Chatfield, University of Oxford.

License: BSD (see LICENSE.md)

Supported platforms
-------------------

Successfully compiled with Ubuntu 14 LTS and macOS Sierra v 10.12.3. See the files in the `install` directory for example installation scripts on each platform.

Installation Instructions
-------------------------

The CMAKE package is used for installation. Ensure you have all dependencies
installed from the section below, and then go to the root folder of vgg_classifier and type:

    $ mkdir build
    $ cd build
    $ cmake -DCaffe_DIR=<caffe-root-dir> -DCaffe_INCLUDE_DIR="<caffe-root-dir>/include;<caffe-root-dir>/build/src" -DLiblinear_DIR=<liblinear-root-dir> -Dcppnetlib_DIR=<cppnetlib-root-dir>/build/ ../
    $ make
    $ make install

If you want to use a specific Boost library, you can replace the third line above with

    $ cmake -DBOOST_ROOT=<boost-root-dir> -DCaffe_DIR=<caffe-root-dir> -DCaffe_INCLUDE_DIR="<caffe-root-dir>/include;<caffe-root-dir>/build/src" -DLiblinear_DIR=<liblinear-root-dir> -Dcppnetlib_DIR=<cppnetlib-root-dir>/build/ ../

The resulting binaries will be installed into the `bin/` subdirectory of the root folder.

As in the original [cpuvisor-srv](https://github.com/kencoken/cpuvisor-srv)  project, the generated executables still use the `cpuvisor-srv` prefix.

Usage
-----

The following preprocessing steps are required before running the service:

 1. Edit `./config.prototxt` with Caffe model files and dataset base paths.
 2. Create/Edit `./dsetpaths.txt` and `./negpaths.txt` with the paths to all dataset and
    negative training images. These files should be referenced in `./config.prototxt`. Sample
    files are provided ( `./dsetpaths_sample.txt` contains all images from the PASCAL VOC 2007 dataset).
 3. Run `./bin/cpuvisor_preproc` to precompute all features.
 4. Run `python legacy_serve.py` and `./bin/cpuvisor_service` to start the backend service that interfaces serves the [vgg_frontend](https://gitlab.com/vgg/vgg_frontend) web application.

Dependencies
------------

#### 1. VGG Image Classifier Service Dependencies

The following C++ libraries are required:

 + [Caffe](http://caffe.berkeleyvision.org/) (successfully tested with rc3)
 + [cppnetlib](https://github.com/kencoken/cpp-netlib) – use the `0.11-devel`
   branch of the forked version of the repo `kencoken/cpp-netlib`
 + Boost v1.55.0 or above (successfully tested with v1.57)
 + [Liblinear](http://www.csie.ntu.edu.tw/~cjlin/liblinear/) (successfully tested with v2.1)
 + ZeroMQ

The following are dependencies shared with Caffe. If you have successfully compiled Caffe these dependencies are already in your system. Otherwise see http://caffe.berkeleyvision.org/install_apt.html.

 + OpenCV
 + Google Logging (GLOG)
 + Google Flags (GFLAGS) v2.1+
 + Google Protobuf
