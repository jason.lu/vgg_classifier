import socket
import json
import os
import sys

SERVE_IP = '127.0.0.1'
SERVE_PORT = 35215
BUFFER_SIZE = 1024
TCP_TERMINATOR = '$$$'

def send_req_obj(socket, req_obj):

    socket.send(json.dumps(req_obj) + TCP_TERMINATOR)

def recv_rep_obj(socket):

    rep_data = socket.recv(BUFFER_SIZE)
    term_idx = rep_data.find(TCP_TERMINATOR)
    while term_idx < 0:
        append_data = socket.recv(BUFFER_SIZE)
        if not append_data:
            raise RuntimeError("No data received!")
        else:
            rep_data = rep_data + append_data
            term_idx = rep_data.find(TCP_TERMINATOR)

    rep_data = rep_data[0:term_idx]

    return json.loads(rep_data)

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print 'Usage: python test_legacy_wrap_no_proto <full_path_of_folder_containing_training_images>'
        sys.exit(0)

    # connect to vgg_classifier service

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((SERVE_IP, SERVE_PORT))

    # send requests

    # 1.- test connection to backend engine
    print 'Sending selfTest'
    req_obj = {'func': 'selfTest'}
    send_req_obj(s, req_obj)
    print 'Received response:'
    rep_obj = recv_rep_obj(s)
    print rep_obj

    # 2.- get a query id from the backend engine
    print 'Sending getQueryId'
    req_obj = {'func': 'getQueryId'}
    send_req_obj(s, req_obj)
    print 'Received response:'
    rep_obj = recv_rep_obj(s)
    print rep_obj

    # 3.- gather paths to training images
    pos_trs_dir = sys.argv[1]
    print 'Scanning ', pos_trs_dir
    pos_trs_paths = [os.path.join(pos_trs_dir, pos_trs_fname) for
                     pos_trs_fname in os.listdir(pos_trs_dir)]

    # 4.- send each image path to the backend engine
    query_id = rep_obj['query_id']
    for pos_trs_path in pos_trs_paths:
        print 'Sending addPosTrs: %s' % pos_trs_path
        req_obj = {'func': 'addPosTrs',
                   'query_id': query_id,
                   'impath': pos_trs_path}
        send_req_obj(s, req_obj)
        print 'Received response:'
        rep_obj = recv_rep_obj(s)
        print rep_obj
        if not rep_obj['success']: raise RuntimeError(rep_obj['err_msg'])

    # 5.- instruct the backend engine to train
    print 'Sending train'
    req_obj = {'func': 'train',
               'query_id': query_id}
    send_req_obj(s, req_obj)
    print 'Received response:'
    rep_obj = recv_rep_obj(s)
    print rep_obj
    if not rep_obj['success']: raise RuntimeError(rep_obj['err_msg'])

    # 6.- instruct the backend engine to rank
    print 'Sending rank'
    req_obj = {'func': 'rank',
               'query_id': query_id}
    send_req_obj(s, req_obj)
    print 'Received response:'
    rep_obj = recv_rep_obj(s)
    print rep_obj
    if not rep_obj['success']: raise RuntimeError(rep_obj['err_msg'])

    # 7.- request ranking from backend
    print 'Sending getRanking'
    req_obj = {'func': 'getRanking',
               'query_id': query_id}
    send_req_obj(s, req_obj)
    print 'Received response:'
    rep_obj = recv_rep_obj(s)
    print rep_obj
    if not rep_obj['success']: raise RuntimeError(rep_obj['err_msg'])

    rank_result = rep_obj

    # 8.- instruct the backend to release the query id
    print 'Sending releaseQueryId'
    req_obj = {'func': 'releaseQueryId',
               'query_id': query_id}
    send_req_obj(s, req_obj)
    print 'Received response:'
    # NOTE: Remember that the backend might return just a subset of the result list, depending on its configuration
    rep_obj = recv_rep_obj(s)
    # comment the line below to avoid the (large) output of the ranking list
    print rep_obj
    if not rep_obj['success']: raise RuntimeError(rep_obj['err_msg'])

    # disconnect from vgg_classifier service
    s.close()

    # save results
    with open('results.txt', 'w+') as outfile:
        json.dump(rank_result['ranklist'], outfile, indent=2)
