vgg_classifier service (docker)
===============================

Author:

 + Ernesto Coto, University of Oxford – <ecoto@robots.ox.ac.uk>

#### *About these instructions*

The instructions below are meant to be executed in a computer with the Linux OS. However, similar instructions can be used for macOS or MS Windows (with or without "sudo", changing the path-separators and using other folder different to $HOME).

#### *Downloading*

The docker version of the service can be found in the VGG docker repository at Docker Hub. It can be downloaded via the terminal by entering:

	docker pull oxvgg/vgg_classifier

After the download you should have a docker image called `oxvgg/vgg_classifier` in your computer.

#### *Sample data*

Sample data can be found at http://www.robots.ox.ac.uk/~vgg/software/vic/downloads/data/vic_data.tar (or http://www.robots.ox.ac.uk/~vgg/software/vic/downloads/data/vic_data.zip)

The `vic_data` file contains a small subset of COCO2014 (100 images), along with an example metadata file and sample negative images. It also contains pre-processed data files for the category search backend, computed out of the sample COCO2014 images.

For the rest of the instructions, it is assumed that you have decompressed `vic_data` in your $HOME directory. Therefore, you should have a folder called `vgg` in your $HOME (i.e. $HOME/vgg should exists).

#### *Starting/Stopping the service*

The service will run by default at port 35215.

The service needs access to: 1) the preprocessed data within the `backend_data` folder, and 2) sample training images within the `frontend_data` folder

NOTE: The training images can be in any folder, as long as the container has access to it. We are using the ones in our `frontend_data` sample data just for convenience.

Start the service and let it run in the background via the terminal by entering:

```
sudo docker run --rm -v $HOME/vgg/vic/backend_data:/webapps/visorgen/backend_data:Z -v $HOME/vgg/vic/frontend_data:$HOME/vgg/vic/frontend_data:Z -p 127.0.0.1:35215:35215 -d oxvgg/vgg_classifier /webapps/visorgen/vgg_classifier/start_service.sh
```

This instruction will start a new oxvgg/vgg_classifier container.

The container can be stop/kill using the docker service.

#### *Performing a query*

Use the `test_legacy_wrap_no_proto.py` script (found in the `test` folder of this repository) to run a query. Just enter in the terminal window:

	python test_legacy_wrap_no_proto.py <training-images-folder>

where <training-images-folder> is the full path to a folder containing the training images to be used for the query.

For instance, given that you have downloaded the sample data, you should be able to run

	python test_legacy_wrap_no_proto.py $HOME/vgg/vic/frontend_data/curatedtrainimgs/cpuvisor-srv/curated__{#car}/positive

The results of the query will be stored in JSON format in `results.txt` in the same directory where the script was executed from.

Note that given the default configuration at https://gitlab.com/vgg/vgg_classifier/blob/master/config.prototxt, a maximum of 10000 results will be returned (as per the `page_size` setting).

NOTE: For MS Windows users, the path with which `test_legacy_wrap_no_proto.py` is executed will be incompatible with the Linux OS inside the docker container. Therefore, we suggest you modify `test_legacy_wrap_no_proto.py` slightly (step 4.-) to convert the paths to Linux-style and redirect them to the folder where the images will be found INSIDE the docker container.

#### *Data ingestion*

New images can be ingested by the service. Please follow the instructions below:

1. Remove the previous files at $HOME/vgg/mydata/images/mydataset/ (but not the folder itself) and place the new files in that same folder.
2. Remove the preprocessed data files from the sample data, i.e, all files starting with `dsetfeats` at $HOME/vgg/vic/backend_data/cpuvisor-srv
3. Create a new file called `dsetpaths.txt` in $HOME/vgg/vic/backend_data/cpuvisor-srv containing the paths to the files to be ingested. The paths should be relative to $HOME/vgg/mydata/images/mydataset/. You will find an example in $HOME/vgg/mydata/dsetpaths.sample.txt
4. Run the following command in the terminal window:

```
sudo docker run --rm -v $HOME/vgg/vic/backend_data:/webapps/visorgen/backend_data:Z -v $HOME/vgg/mydata/images/mydataset:/webapps/visorgen/datasets/images/mydataset:Z -it oxvgg/vgg_classifier /webapps/visorgen/vgg_classifier/bin/cpuvisor_preproc -config_path /webapps/visorgen/vgg_classifier/config.prototxt -dsetfeats
```

The data ingestion process should start and you should be able to follow it with the print outs in the screen.

You can also leave the process running in the background by replacing the `-it` option in the command above by `-d`. The container should be removed automatically once the process is finished.

#### *Negative images*

New NEGATIVE images can be ingested by the service as well. Please follow the instructions below:

1. Remove the previous files at $HOME/vgg/mydata/negatives/mydataset/ (but not the folder itself) and place the new files in that same folder.
2. Remove the preprocessed data files from the sample data, i.e, all files starting with `negfeats` at $HOME/vgg/vic/backend_data/cpuvisor-srv
3. Create a new file called `negpaths.txt` in $HOME/vgg/vic/backend_data/cpuvisor-srv containing the paths to the files to be ingested. The paths should be relative to $HOME/vgg/mydata/negatives/mydataset/. You will find an example in $HOME/vgg/mydata/negpaths.sample.txt
4. Run the following command in the terminal window:

```
sudo docker run --rm -v $HOME/vgg/vic/backend_data:/webapps/visorgen/backend_data:Z -v $HOME/vgg/mydata/negatives/mydataset:/webapps/visorgen/datasets/negatives/mydataset:Z -it oxvgg/vgg_classifier /webapps/visorgen/vgg_classifier/bin/cpuvisor_preproc -config_path /webapps/visorgen/vgg_classifier/config.prototxt -negfeats
```

The ingestion of negative images should start and you should be able to follow it with the print outs in the screen.

You can also leave the process running in the background by replacing the `-it` option in the command above by `-d`. The container should be removed automatically once the process is finished.

